﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;
using OfficeOpenXml;
using OfficeOpenXml.Drawing;
using OfficeOpenXml.Style;
using System.Data.OleDb;

/* Form1 is for the interpreter main class for parsing, browsing and export*/
namespace Parsing_File
{    
    public partial class Form1 : Form
    {
        List<string> listFiles = new List<string>();        
        DataTable dt_Source = new DataTable(); /* Table for datagrid view i.e GUI display */       

        public Form1()
        {
            InitializeComponent();
        }

        /* Load the Form for the Data Logging interpreter tool */
        private void Form1_Load(object sender, EventArgs e)
        {
            button3.Enabled = false;
            //button1.Enabled = false;
            if (listView.FocusedItem != null)
            {
                button3.Enabled = true;
            }
        }               
              
        /* Browse the S19 file, where S19 file is stored in user system */
        private void btnOpen_Click(object sender, EventArgs e)
        {
            listFiles.Clear();
            listView.Items.Clear();            
            using (FolderBrowserDialog fbd = new FolderBrowserDialog() { Description = "Select your path." })
            {
                fbd.SelectedPath = Properties.Settings.Default.LastSelectedFolder;                              
                if (fbd.ShowDialog() == DialogResult.OK)
                {
                    txtPath.Text = fbd.SelectedPath;                    
                    Properties.Settings.Default.LastSelectedFolder = fbd.SelectedPath.ToString();
                    Properties.Settings.Default.Save();
                    foreach (string item in Directory.GetFiles(fbd.SelectedPath, "*.*", SearchOption.AllDirectories).Where(s => s.EndsWith(".S19") ||
                                                                                                                                s.EndsWith(".s19")))
                    {
                        listFiles.Add(item);
                        imageList.Images.Add(System.Drawing.Icon.ExtractAssociatedIcon(item));
                        FileInfo fi = new FileInfo(item);
                        listFiles.Add(fi.FullName);
                        listView.Items.Add(fi.Name, imageList.Images.Count - 1);
                    }   
                    
                }
            } 
        }


        /* List all the files present in the user selected directory */ 
        private void listView_SelectedIndexChanged(object sender, EventArgs e)
        {
            
            var supportedTypes = new[] {"S19"};         
          
            if (listView.FocusedItem != null)
            {
                button3.Enabled = true;
            }
        } 
        
        /* Once we click on required sheets, it will display respective sheet in datagridview i.e GUI */
        private void SheetButtonClick_SheetChanged(object sender, EventArgs e, ExcelPackage globalPackage)
        {
            System.Windows.Forms.Button button = (Button)sender;
            string sheetname = button.Text;            
            ExcelWorksheet globalLastActiveWorksheet = (ExcelWorksheet)globalPackage.Workbook.Worksheets[sheetname];                        
            filldatagridview(globalLastActiveWorksheet);
        }

        /* Add all worksheet buttons in Flow Layout Panel */
        public void AddSheetButtons(ExcelPackage globalPackage)
        {
            flowLayoutPanel1.Controls.Clear();            
            for (int i = 1; i <= globalPackage.Workbook.Worksheets.Count; i++)
            {
                System.Windows.Forms.Button SheetButton = new System.Windows.Forms.Button();
                SheetButton.Text = globalPackage.Workbook.Worksheets[i].Name.ToString();                
                SheetButton.Click += delegate (object sender, EventArgs e) {SheetButtonClick_SheetChanged(sender, e, globalPackage);};                
                SheetButton.AutoSize = true;
                flowLayoutPanel1.Controls.Add(SheetButton);  
            }            
        }

        /* Convert selected worksheet in DataTable format */
        public System.Data.DataTable WorksheetToDataTable(ExcelWorksheet oSheet)
        {
            DataTable dt;
            try
            {
                if ((oSheet.Dimension == null))
                    dt = new DataTable();
                else
                {
                    int totalRows = oSheet.Dimension.End.Row;
                    int totalCols = oSheet.Dimension.End.Column;
                    dt = new DataTable(oSheet.Name);
                    DataRow dr = null; /* TODO Change to default(_) if this is not a reference type */
                    for (int i = 1; i <= totalCols; i++)
                        dt.Columns.Add("Column_" + i.ToString());// adding custom column names
                    for (int i = 1; i <= totalRows; i++)
                    {
                        dr = dt.Rows.Add();
                        for (int j = 1; j <= totalCols; j++)
                            dr[j - 1] = oSheet.Cells[i, j].Value;
                    }
                }
            }
            catch (Exception lException)
            {
                MessageBox.Show(lException.Message);
                return new DataTable();
            }
            return dt;
        }

        /* Put the DataTable in Datagridview i.e GUI */
        public void filldatagridview(ExcelWorksheet lWorksheet)
        {            
            dt_Source = WorksheetToDataTable(lWorksheet);
            if (dt_Source.Rows.Count == 0)
                MessageBox.Show("There is no data in the selected sheet");
            dataGridView1.DataSource = dt_Source;
        }

        /* Create Datagridview i.e GUI for first worksheet */
        private void CreateDataGridSource(string fileName)
        {            
            try
            {
                FileInfo File = new FileInfo(fileName);                
                ExcelPackage globalPackage = new ExcelPackage(File);
                string  globalFileName = globalPackage.File.FullName;                
                var globalLastActiveWorksheet = globalPackage.Workbook.Worksheets.First();
                filldatagridview(globalLastActiveWorksheet);
                AddSheetButtons(globalPackage);
            }
            catch (Exception e)
            {
                if (e.Message == @"Can not open the package. Package is an OLE compound document. _
                            If this is an encrypted package, please supply the password")
                    MessageBox.Show(@"Please check the file format, .xls files are not supported, _
                      use .xlsx format!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                else
                    MessageBox.Show(e.Message, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }


        /// Handles the Click event of the btnExport control. Export the data to excel.    
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void button3_Click(object sender, EventArgs e)
        {
            dataGridView1.DataSource = null;
            dataGridView1.Refresh();
            flowLayoutPanel1.Controls.Clear();
            try
            {
                /*File Decode */
                Int32 fileSize = 787456;
                UInt32 startAddress = 0x080C0000;// Start address for section 1
                UInt32 startAddressSec02 = 0x080E0000;// Start address for section 2
                string strA = "\\";
                string path = String.Concat(strA, listView.FocusedItem.Text);
                Thread t;
                if (path != null)
                {
                    //frmWaitForm displays the "Loading" form                   
                    frmWaitForm popup = new frmWaitForm(); //create instance
                    t = new Thread(() => popup.ShowDialog());
                    t.Start(); 
                    new readfile(String.Concat(txtPath.Text, path),
                                 fileSize, startAddress); /* Read the S19 file for parsing */

                  /*  if ((path != null) & (readfile.address >= 0x080C0000))
                    {
                        button1.Enabled = true;
                    }*/

                    if (readfile.address >= 0x080C0000)
                    {
                        GenerateReport(); //Create excel file and display in GUI.
                    }
                    t.Abort(); //abort the frmWaitForm thread (close the form)
                    t.Join();
                    button3.Enabled = false;
                }
            }

            catch(Exception)
            {
                Thread.ResetAbort();
            }
        }



        /// Generates the report.
        private static void GenerateReport()
        {
            using (ExcelPackage p = new ExcelPackage())
            {
                //set the workbook properties and add a default sheet in it
                SetWorkbookProperties(p);
                int ix = 0;
                for (int i = 0; i < 54; i++)
                {
                    if (i < 2)
                    {
                        ExcelWorksheet ws = CreateSheet(p, "HeaderSec" + (i + 1), (i + 1)); //Create sheets for headers of section 1 and 2
                        if (i == 0)
                        {
                            DataTable dt = CreateDataTablesec1(i); //My Function which generates DataTable
                                                                   //Merging cells and create a center heading for out table
                            ws.Cells[1, 1].Value = "S19 File Header";
                            ws.Cells[1, 1, 1, dt.Columns.Count].Merge = true;
                            ws.Cells[1, 1, 1, dt.Columns.Count].Style.Font.Bold = true;
                            ws.Cells[1, 1, 1, dt.Columns.Count].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            int rowIndex = 2;

                            CreateHeadersec(ws, ref rowIndex, dt);
                            CreateData(ws, ref rowIndex, dt);
                        }
                        else
                        {
                            DataTable dt = CreateDataTablesec2(i); //My Function which generates DataTable
                                                                   //Merging cells and create a center heading for out table
                            ws.Cells[1, 1].Value = "S19 File Header";
                            ws.Cells[1, 1, 1, dt.Columns.Count].Merge = true;
                            ws.Cells[1, 1, 1, dt.Columns.Count].Style.Font.Bold = true;
                            ws.Cells[1, 1, 1, dt.Columns.Count].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            int rowIndex = 2;

                            CreateHeadersec(ws, ref rowIndex, dt);
                            CreateData(ws, ref rowIndex, dt);
                        }
                       
                    }              
                    else if ((i > 1) && (i < 28))
                    {
                        ix = i;
                        int z = i - 2;
                        ExcelWorksheet ws = CreateSheet(p, "DataRecordSecA" + (z + 1), (ix + 1));//Create sheets for section 1
                        DataTable dt = CreateDataTable(z); //My Function which generates DataTable

                        //Merging cells and create a center heading for out table
                        ws.Cells[1, 1].Value = "S19 File";
                        ws.Cells[1, 1, 1, dt.Columns.Count].Merge = true;
                        ws.Cells[1, 1, 1, dt.Columns.Count].Style.Font.Bold = true;
                        ws.Cells[1, 1, 1, dt.Columns.Count].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        int rowIndex = 2;

                        CreateHeader(ws, ref rowIndex, dt);
                        CreateData(ws, ref rowIndex, dt);                        
                    }
                    else
                    {
                        ix = i;
                        int x = i - 28;
                        ExcelWorksheet ws = CreateSheet(p, "DataRecordSecB" + (x + 1), (ix + 1));//Create sheets for section 2
                        DataTable dt = CreateDataTableB(x); //My Function which generates DataTable

                        //Merging cells and create a center heading for out table
                        ws.Cells[1, 1].Value = "S19 File";
                        ws.Cells[1, 1, 1, dt.Columns.Count].Merge = true;
                        ws.Cells[1, 1, 1, dt.Columns.Count].Style.Font.Bold = true;
                        ws.Cells[1, 1, 1, dt.Columns.Count].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        int rowIndex = 2;

                        CreateHeader(ws, ref rowIndex, dt);//Create parameters for 1st column
                        CreateData(ws, ref rowIndex, dt); //Assign data to the related cells
                    }
                    

                }

                
                //Generate A File with same name as S19 file name
                Byte[] bin = p.GetAsByteArray();
                
                string file = System.IO.Path.GetFileNameWithoutExtension(Program.form1.listView.FocusedItem.Text) + ".xlsx";
                

                string strA = "\\";      
                string path = String.Concat(strA, file);//concatenate "\\" with filename

                string pathFile = String.Concat(Program.form1.txtPath.Text, path);// Program.form1.txtPath.Text is directory of excel file
                try
                {
                    File.WriteAllBytes(pathFile, bin);  //Copy data to excel file in S19 file directory
                }
                catch(Exception)
                {
                    MessageBox.Show("Unable to write to excel file, Please close the opened file");
                }   
                
                Program.form1.CreateDataGridSource(pathFile);
                Program.form1.dataGridView1.DataSource = Program.form1.dt_Source;
                MessageBox.Show("Click on required Worksheet button from Panel or Click on Open Excel File button");
            }
        }

        
        /* Create worksheet */
        private static ExcelWorksheet CreateSheet(ExcelPackage p, string sheetName, int ix)
        {
            
            p.Workbook.Worksheets.Add(sheetName);
            ExcelWorksheet ws = p.Workbook.Worksheets[ix];

            ws.Name = sheetName; //Setting Sheet's name
            ws.Cells.Style.Font.Size = 11; //Default font size for whole sheet
            ws.Cells.Style.Font.Name = "Calibri"; //Default Font name for whole sheet

            return ws;
        }

 
        /// <summary>
        /// Sets the workbook properties and adds a default sheet.
        /// </summary>
        /// <param name="p">The p.</param>
        /// <returns></returns>
        private static void SetWorkbookProperties(ExcelPackage p)
        {
            //Here setting some document properties
            p.Workbook.Properties.Author = "Sharnabasappa";
            p.Workbook.Properties.Title = "EPPlus Sample for Interpreter";


        }

        /*Create header parameters for all sheets for sec1 and sec2*/
        private static void CreateHeadersec(ExcelWorksheet ws, ref int rowIndex, DataTable dt)
        {
            ws.Cells[2, 1].Value = "Parameters";
            var fill1 = ws.Cells[2, 1].Style.Fill;
            fill1.PatternType = ExcelFillStyle.Solid;
            fill1.BackgroundColor.SetColor(Color.Gray);
            var border1 = ws.Cells[2, 1].Style.Border;
            border1.Bottom.Style = border1.Top.Style = border1.Left.Style = border1.Right.Style = ExcelBorderStyle.Thin;
            ws.Cells[3, 1].Value = "BMM PN";
            ws.Cells[4, 1].Value = "BMM SN";
            ws.Cells[5, 1].Value = "SMU Count";
            for (int m = 0; m < 40; m++)
            {
                ws.Cells[6 + m, 1].Value = "SMU SN Table" + m;
            }

            for (int m = 0; m < 11; m++)
            {
                ws.Cells[46 + m, 1].Value = "Battery PN" + m;
            }

            for (int m = 0; m < 15; m++)
            {
                ws.Cells[57 + m, 1].Value = "Battery SN" + m;
            }

            for (int m = 0; m < 3; m++)
            {
                ws.Cells[72 + m, 1].Value = "SW Version" + m;
            }

            int colIndex = 2;
            foreach (DataColumn dc in dt.Columns) //Creating Headings
            {
                var cell = ws.Cells[rowIndex, colIndex];

                //Setting the background color of header cells to Gray
                var fill = cell.Style.Fill;
                fill.PatternType = ExcelFillStyle.Solid;
                fill.BackgroundColor.SetColor(Color.Gray);

                //Setting Top/left,right/bottom borders.
                var border = cell.Style.Border;
                border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;

                //Setting Value in cell                
                int x = Convert.ToInt32(dc.ColumnName);
                cell.Value = "record" + (x + 1);

                colIndex++;
            }
        }

        /*Assign data to all parameters for section 1 header*/
        private static DataTable CreateDataTablesec1(int x)
        {
            DataTable dt = new DataTable();
            for (int i = 0; i < 1; i++)
            {
                dt.Columns.Add(i.ToString());
            }

            {
                DataRow dr = dt.NewRow();
                for (int m = 0; m < 1; m++)
                {
                    DataColumn dc = dt.Columns[m];                   
                    dr[dc.ToString()] = MemoryParser.MemoryMuncher.Bmm_pn[m];  
                }
                dt.Rows.Add(dr);
            }

            {
                DataRow dr = dt.NewRow();
                for (int m = 0; m < 1; m++)
                {
                    DataColumn dc = dt.Columns[m]; 
                    dr[dc.ToString()] = MemoryParser.MemoryMuncher.Bmm_sn[m];                     
                }
                dt.Rows.Add(dr);
            }

            {
                DataRow dr = dt.NewRow();
                for (int m = 0; m < 1; m++)
                {
                    DataColumn dc = dt.Columns[m];
                    dr[dc.ToString()] = MemoryParser.MemoryMuncher.Smu_count[m]; 
                }
                dt.Rows.Add(dr);
            }

            {
                for (int i = 0; i < 40; i++)
                {
                    DataRow dr = dt.NewRow();
                    for (int m = 0; m < 1; m++)
                    {
                        DataColumn dc = dt.Columns[m];
                        dr[dc.ToString()] = MemoryParser.MemoryMuncher.Smu_sn_Table[i, m];
                    }
                    dt.Rows.Add(dr);
                }
            }

            {
                for (int i = 0; i < 11; i++)
                {
                    DataRow dr = dt.NewRow();
                    for (int m = 0; m < 1; m++)
                    {
                        DataColumn dc = dt.Columns[m];
                        dr[dc.ToString()] = MemoryParser.MemoryMuncher.Battery_pn[i, m];
                    }
                    dt.Rows.Add(dr);
                }
            }

            {
                for (int i = 0; i < 15; i++)
                {
                    DataRow dr = dt.NewRow();
                    for (int m = 0; m < 1; m++)
                    {
                        DataColumn dc = dt.Columns[m];
                        dr[dc.ToString()] = MemoryParser.MemoryMuncher.Battery_sn[i, m];
                    }
                    dt.Rows.Add(dr);
                }
            }

            {
                for (int i = 0; i < 3; i++)
                {
                    DataRow dr = dt.NewRow();
                    for (int m = 0; m < 1; m++)
                    {
                        DataColumn dc = dt.Columns[m];
                        dr[dc.ToString()] = MemoryParser.MemoryMuncher.Sw_version[i, m];
                    }
                    dt.Rows.Add(dr);
                }
            }
            return dt;
        }

        /*Assign data to all parameters for section 2 header*/
        private static DataTable CreateDataTablesec2(int x)
        {
            DataTable dt = new DataTable();
            for (int i = 0; i < 1; i++)
            {
                dt.Columns.Add(i.ToString());
            }

            {
                DataRow dr = dt.NewRow();
                for (int m = 0; m < 1; m++)
                {
                    DataColumn dc = dt.Columns[m];
                    dr[dc.ToString()] = MemoryParser.MemoryMuncherB.Bmm_pn[m];
                }
                dt.Rows.Add(dr);
            }

            {
                DataRow dr = dt.NewRow();
                for (int m = 0; m < 1; m++)
                {
                    DataColumn dc = dt.Columns[m];
                    dr[dc.ToString()] = MemoryParser.MemoryMuncherB.Bmm_sn[m];
                }
                dt.Rows.Add(dr);
            }

            {
                DataRow dr = dt.NewRow();
                for (int m = 0; m < 1; m++)
                {
                    DataColumn dc = dt.Columns[m];
                    dr[dc.ToString()] = MemoryParser.MemoryMuncherB.Smu_count[m];
                }
                dt.Rows.Add(dr);
            }

            {
                for (int i = 0; i < 40; i++)
                {
                    DataRow dr = dt.NewRow();
                    for (int m = 0; m < 1; m++)
                    {
                        DataColumn dc = dt.Columns[m];
                        dr[dc.ToString()] = MemoryParser.MemoryMuncherB.Smu_sn_Table[i, m];
                    }
                    dt.Rows.Add(dr);
                }
            }

            {
                for (int i = 0; i < 11; i++)
                {
                    DataRow dr = dt.NewRow();
                    for (int m = 0; m < 1; m++)
                    {
                        DataColumn dc = dt.Columns[m];
                        dr[dc.ToString()] = MemoryParser.MemoryMuncherB.Battery_pn[i, m];
                    }
                    dt.Rows.Add(dr);
                }
            }

            {
                for (int i = 0; i < 15; i++)
                {
                    DataRow dr = dt.NewRow();
                    for (int m = 0; m < 1; m++)
                    {
                        DataColumn dc = dt.Columns[m];
                        dr[dc.ToString()] = MemoryParser.MemoryMuncherB.Battery_sn[i, m];
                    }
                    dt.Rows.Add(dr);
                }
            }

            {
                for (int i = 0; i < 3; i++)
                {
                    DataRow dr = dt.NewRow();
                    for (int m = 0; m < 1; m++)
                    {
                        DataColumn dc = dt.Columns[m];
                        dr[dc.ToString()] = MemoryParser.MemoryMuncherB.Sw_version[i, m];
                    }
                    dt.Rows.Add(dr);
                }
            }
            return dt;
        }

        /*Create parameters for section 1 and 2 i.e operational data*/
        private static void CreateHeader(ExcelWorksheet ws, ref int rowIndex, DataTable dt)
        {
            ws.Cells[2, 1].Value = "Parameters";
            var fill1 = ws.Cells[2, 1].Style.Fill;
            fill1.PatternType = ExcelFillStyle.Solid;
            fill1.BackgroundColor.SetColor(Color.Gray);
            var border1 = ws.Cells[2, 1].Style.Border;
            border1.Bottom.Style = border1.Top.Style = border1.Left.Style = border1.Right.Style = ExcelBorderStyle.Thin;
            ws.Cells[3, 1].Value = "Time Stamp (RTC Value)";
            ws.Cells[4, 1].Value = "BMM Mode";
            ws.Cells[5, 1].Value = "Triggering Event";
            ws.Cells[6, 1].Value = "Event Activation Counter";
            ws.Cells[7, 1].Value = "SOC Min";
            ws.Cells[8, 1].Value = "SOC Max";
            ws.Cells[9, 1].Value = "SOC Avg";
            ws.Cells[10, 1].Value = "SOH";
            ws.Cells[11, 1].Value = "Current Avg High Range";
            ws.Cells[12, 1].Value = "Current Avg Low Range";
            ws.Cells[13, 1].Value = "Amp Throughput High Range";
            ws.Cells[14, 1].Value = "Amp Throughput Low Range";
            ws.Cells[15, 1].Value = "Battery (String) Voltage";
            ws.Cells[16, 1].Value = "Max Cell Voltage";
            ws.Cells[17, 1].Value = "Module Identifier for Vmax";
            ws.Cells[18, 1].Value = "Min Cell Voltage";
            ws.Cells[19, 1].Value = "Module Identifier for Vmin";
            ws.Cells[20, 1].Value = "Max Temp";
            ws.Cells[21, 1].Value = "Module Identifier for Tmax";
            ws.Cells[22, 1].Value = "Min Temp";
            ws.Cells[23, 1].Value = "Module Identifier for Tmin";
            ws.Cells[24, 1].Value = "Average Temperature";
            for (int m = 0; m < 10; m++)
            {
                ws.Cells[25+m, 1].Value = "Alarms/Faults"+m;
            }
            ws.Cells[35, 1].Value = "PBIT";
            ws.Cells[36, 1].Value = "CBIT";
            ws.Cells[37, 1].Value = "IBIT";
            ws.Cells[38, 1].Value = "POffBIT";
            ws.Cells[39, 1].Value = "Contactor Status Byte";

            int colIndex = 2;
            foreach (DataColumn dc in dt.Columns) //Creating Headings
            {
                var cell = ws.Cells[rowIndex, colIndex];

                //Setting the background color of header cells to Gray
                var fill = cell.Style.Fill;
                fill.PatternType = ExcelFillStyle.Solid;
                fill.BackgroundColor.SetColor(Color.Gray);

                //Setting Top/left,right/bottom borders.
                var border = cell.Style.Border;
                border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;

                //Setting Value in cell                
                int x = Convert.ToInt32(dc.ColumnName);
                cell.Value = "record" + (x + 1);

                colIndex++;
            }
        }
        /*Adding Data into rows and columns for section 1 and 2 opeartional data*/
        private static void CreateData(ExcelWorksheet ws, ref int rowIndex, DataTable dt)
        {
            int colIndex = 0;
            foreach (DataRow dr in dt.Rows) // Adding Data into rows
            {
                colIndex = 2;
                rowIndex++;

                foreach (DataColumn dc in dt.Columns)
                {
                    var cell = ws.Cells[rowIndex, colIndex];

                    //Setting Value in cell                    
                    cell.Value = Convert.ToString(dr[dc.ColumnName]);

                    //Setting borders of cell
                    var border = cell.Style.Border;
                    border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;
                    colIndex++;
                }
            }
        }
                                  

        /// <summary>
        /// Creates the data table with data for section 1.
        /// </summary>
        /// <returns>DataTable</returns>
        private static DataTable CreateDataTable(int x)
        {
            DataTable dt = new DataTable();
            for (int i = 0; i < 60; i++)
            {
                dt.Columns.Add(i.ToString());
            }


            {
                DataRow dr = dt.NewRow();
                for (int m = 0; m < 60; m++)
                {
                    DataColumn dc = dt.Columns[m];
                    dr[dc.ToString()] = MemoryParser.MemoryMuncher.Time_stamp[x, m];
                }
                dt.Rows.Add(dr);
            }


            {
                DataRow dr = dt.NewRow();
                for (int m = 0; m < 60; m++)
                {
                    DataColumn dc = dt.Columns[m];
                    dr[dc.ToString()] = MemoryParser.MemoryMuncher.Bmm_mode[x, m];
                }
                dt.Rows.Add(dr);
            }

            {
                DataRow dr = dt.NewRow();
                for (int m = 0; m < 60; m++)
                {
                    DataColumn dc = dt.Columns[m];
                    dr[dc.ToString()] = MemoryParser.MemoryMuncher.Triggering_event[x, m];
                }
                dt.Rows.Add(dr);
            }

            {
                DataRow dr = dt.NewRow();
                for (int m = 0; m < 60; m++)
                {
                    DataColumn dc = dt.Columns[m];
                    dr[dc.ToString()] = MemoryParser.MemoryMuncher.Event_act_Counter[x, m];
                }
                dt.Rows.Add(dr);
            }

            {
                DataRow dr = dt.NewRow();
                for (int m = 0; m < 60; m++)
                {
                    DataColumn dc = dt.Columns[m];
                    dr[dc.ToString()] = MemoryParser.MemoryMuncher.Soc_min[x, m];
                }
                dt.Rows.Add(dr);
            }

            {
                DataRow dr = dt.NewRow();
                for (int m = 0; m < 60; m++)
                {
                    DataColumn dc = dt.Columns[m];
                    dr[dc.ToString()] = MemoryParser.MemoryMuncher.Soc_max[x, m];
                }
                dt.Rows.Add(dr);
            }

            {
                DataRow dr = dt.NewRow();
                for (int m = 0; m < 60; m++)
                {
                    DataColumn dc = dt.Columns[m];
                    dr[dc.ToString()] = MemoryParser.MemoryMuncher.Soc_avg[x, m];
                }
                dt.Rows.Add(dr);
            }

            {
                DataRow dr = dt.NewRow();
                for (int m = 0; m < 60; m++)
                {
                    DataColumn dc = dt.Columns[m];
                    dr[dc.ToString()] = MemoryParser.MemoryMuncher.Soh[x, m];
                }
                dt.Rows.Add(dr);
            }

            {
                DataRow dr = dt.NewRow();
                for (int m = 0; m < 60; m++)
                {
                    DataColumn dc = dt.Columns[m];
                    dr[dc.ToString()] = MemoryParser.MemoryMuncher.Cur_Avg_High_Range[x, m];
                }
                dt.Rows.Add(dr);
            }

            {
                DataRow dr = dt.NewRow();
                for (int m = 0; m < 60; m++)
                {
                    DataColumn dc = dt.Columns[m];
                    dr[dc.ToString()] = MemoryParser.MemoryMuncher.Cur_Avg_Low_Range[x, m];
                }
                dt.Rows.Add(dr);
            }

            {
                DataRow dr = dt.NewRow();
                for (int m = 0; m < 60; m++)
                {
                    DataColumn dc = dt.Columns[m];
                    dr[dc.ToString()] = MemoryParser.MemoryMuncher.Amp_throughput_highrange[x, m];
                }
                dt.Rows.Add(dr);
            }

            {
                DataRow dr = dt.NewRow();
                for (int m = 0; m < 60; m++)
                {
                    DataColumn dc = dt.Columns[m];
                    dr[dc.ToString()] = MemoryParser.MemoryMuncher.Amp_throughput_lowrange[x, m];
                }
                dt.Rows.Add(dr);
            }

            {
                DataRow dr = dt.NewRow();
                for (int m = 0; m < 60; m++)
                {
                    DataColumn dc = dt.Columns[m];
                    dr[dc.ToString()] = MemoryParser.MemoryMuncher.Battery_voltage[x, m];
                }
                dt.Rows.Add(dr);
            }

            {
                DataRow dr = dt.NewRow();
                for (int m = 0; m < 60; m++)
                {
                    DataColumn dc = dt.Columns[m];
                    dr[dc.ToString()] = MemoryParser.MemoryMuncher.Max_cell_volt[x, m];
                }
                dt.Rows.Add(dr);
            }

            {
                DataRow dr = dt.NewRow();
                for (int m = 0; m < 60; m++)
                {
                    DataColumn dc = dt.Columns[m];
                    dr[dc.ToString()] = MemoryParser.MemoryMuncher.Module_Identifier_Vmax[x, m];
                }
                dt.Rows.Add(dr);
            }

            {
                DataRow dr = dt.NewRow();
                for (int m = 0; m < 60; m++)
                {
                    DataColumn dc = dt.Columns[m];
                    dr[dc.ToString()] = MemoryParser.MemoryMuncher.Min_cell_voltage[x, m];
                }
                dt.Rows.Add(dr);
            }

            {
                DataRow dr = dt.NewRow();
                for (int m = 0; m < 60; m++)
                {
                    DataColumn dc = dt.Columns[m];
                    dr[dc.ToString()] = MemoryParser.MemoryMuncher.Module_Identifier_Vmin[x, m];
                }
                dt.Rows.Add(dr);
            }

            {
                DataRow dr = dt.NewRow();
                for (int m = 0; m < 60; m++)
                {
                    DataColumn dc = dt.Columns[m];
                    dr[dc.ToString()] = MemoryParser.MemoryMuncher.Max_temp[x, m];
                }
                dt.Rows.Add(dr);
            }

            {
                DataRow dr = dt.NewRow();
                for (int m = 0; m < 60; m++)
                {
                    DataColumn dc = dt.Columns[m];
                    dr[dc.ToString()] = MemoryParser.MemoryMuncher.Module_Identifier_Tmax[x, m];
                }
                dt.Rows.Add(dr);
            }

            {
                DataRow dr = dt.NewRow();
                for (int m = 0; m < 60; m++)
                {
                    DataColumn dc = dt.Columns[m];
                    dr[dc.ToString()] = MemoryParser.MemoryMuncher.Min_temp[x, m];
                }
                dt.Rows.Add(dr);
            }

            {
                DataRow dr = dt.NewRow();
                for (int m = 0; m < 60; m++)
                {
                    DataColumn dc = dt.Columns[m];
                    dr[dc.ToString()] = MemoryParser.MemoryMuncher.Module_Identifier_Tmin[x, m];
                }
                dt.Rows.Add(dr);
            }

            {
                DataRow dr = dt.NewRow();
                for (int m = 0; m < 60; m++)
                {
                    DataColumn dc = dt.Columns[m];
                    dr[dc.ToString()] = MemoryParser.MemoryMuncher.Avg_temp[x, m];
                }
                dt.Rows.Add(dr);
            }

            {
                for (int i = 0; i < 10; i++)
                {
                    DataRow dr = dt.NewRow();
                    for (int m = 0; m < 60; m++)
                    {
                        DataColumn dc = dt.Columns[m];
                        dr[dc.ToString()] = MemoryParser.MemoryMuncher.alarms[i, x, m];
                    }                
                    dt.Rows.Add(dr);
                }
            }

            {
                DataRow dr = dt.NewRow();
                for (int m = 0; m < 60; m++)
                {
                    DataColumn dc = dt.Columns[m];
                    dr[dc.ToString()] = MemoryParser.MemoryMuncher.Pbit[x, m];
                }
                dt.Rows.Add(dr);
            }

            {
                DataRow dr = dt.NewRow();
                for (int m = 0; m < 60; m++)
                {
                    DataColumn dc = dt.Columns[m];
                    dr[dc.ToString()] = MemoryParser.MemoryMuncher.Cbit[x, m];
                }
                dt.Rows.Add(dr);
            }

            {
                DataRow dr = dt.NewRow();
                for (int m = 0; m < 60; m++)
                {
                    DataColumn dc = dt.Columns[m];
                    dr[dc.ToString()] = MemoryParser.MemoryMuncher.Ibit[x, m];
                }
                dt.Rows.Add(dr);
            }

            {
                DataRow dr = dt.NewRow();
                for (int m = 0; m < 60; m++)
                {
                    DataColumn dc = dt.Columns[m];
                    dr[dc.ToString()] = MemoryParser.MemoryMuncher.POffBit[x, m];
                }
                dt.Rows.Add(dr);
            }

            {
                DataRow dr = dt.NewRow();
                for (int m = 0; m < 60; m++)
                {
                    DataColumn dc = dt.Columns[m];
                    dr[dc.ToString()] = MemoryParser.MemoryMuncher.Contactor_status_main_pos[x, m];
                }
                dt.Rows.Add(dr);
            }
            return dt;
        }

        /*Creates the data table with data for section 2.*/
        private static DataTable CreateDataTableB(int x)
        {
            DataTable dt = new DataTable();
            for (int i = 0; i < 60; i++)
            {
                dt.Columns.Add(i.ToString());
            }


            {
                DataRow dr = dt.NewRow();
                for (int m = 0; m < 60; m++)
                {
                    DataColumn dc = dt.Columns[m];
                    dr[dc.ToString()] = MemoryParser.MemoryMuncherB.Time_stamp[x, m];
                }
                dt.Rows.Add(dr);
            }


            {
                DataRow dr = dt.NewRow();
                for (int m = 0; m < 60; m++)
                {
                    DataColumn dc = dt.Columns[m];
                    dr[dc.ToString()] = MemoryParser.MemoryMuncherB.Bmm_mode[x, m];
                }
                dt.Rows.Add(dr);
            }

            {
                DataRow dr = dt.NewRow();
                for (int m = 0; m < 60; m++)
                {
                    DataColumn dc = dt.Columns[m];
                    dr[dc.ToString()] = MemoryParser.MemoryMuncherB.Triggering_event[x, m];
                }
                dt.Rows.Add(dr);
            }

            {
                DataRow dr = dt.NewRow();
                for (int m = 0; m < 60; m++)
                {
                    DataColumn dc = dt.Columns[m];
                    dr[dc.ToString()] = MemoryParser.MemoryMuncherB.Event_act_Counter[x, m];
                }
                dt.Rows.Add(dr);
            }

            {
                DataRow dr = dt.NewRow();
                for (int m = 0; m < 60; m++)
                {
                    DataColumn dc = dt.Columns[m];
                    dr[dc.ToString()] = MemoryParser.MemoryMuncherB.Soc_min[x, m];
                }
                dt.Rows.Add(dr);
            }

            {
                DataRow dr = dt.NewRow();
                for (int m = 0; m < 60; m++)
                {
                    DataColumn dc = dt.Columns[m];
                    dr[dc.ToString()] = MemoryParser.MemoryMuncherB.Soc_max[x, m];
                }
                dt.Rows.Add(dr);
            }

            {
                DataRow dr = dt.NewRow();
                for (int m = 0; m < 60; m++)
                {
                    DataColumn dc = dt.Columns[m];
                    dr[dc.ToString()] = MemoryParser.MemoryMuncherB.Soc_avg[x, m];
                }
                dt.Rows.Add(dr);
            }

            {
                DataRow dr = dt.NewRow();
                for (int m = 0; m < 60; m++)
                {
                    DataColumn dc = dt.Columns[m];
                    dr[dc.ToString()] = MemoryParser.MemoryMuncherB.Soh[x, m];
                }
                dt.Rows.Add(dr);
            }

            {
                DataRow dr = dt.NewRow();
                for (int m = 0; m < 60; m++)
                {
                    DataColumn dc = dt.Columns[m];
                    dr[dc.ToString()] = MemoryParser.MemoryMuncherB.Cur_Avg_High_Range[x, m];
                }
                dt.Rows.Add(dr);
            }

            {
                DataRow dr = dt.NewRow();
                for (int m = 0; m < 60; m++)
                {
                    DataColumn dc = dt.Columns[m];
                    dr[dc.ToString()] = MemoryParser.MemoryMuncherB.Cur_Avg_Low_Range[x, m];
                }
                dt.Rows.Add(dr);
            }

            {
                DataRow dr = dt.NewRow();
                for (int m = 0; m < 60; m++)
                {
                    DataColumn dc = dt.Columns[m];
                    dr[dc.ToString()] = MemoryParser.MemoryMuncherB.Amp_throughput_highrange[x, m];
                }
                dt.Rows.Add(dr);
            }

            {
                DataRow dr = dt.NewRow();
                for (int m = 0; m < 60; m++)
                {
                    DataColumn dc = dt.Columns[m];
                    dr[dc.ToString()] = MemoryParser.MemoryMuncherB.Amp_throughput_lowrange[x, m];
                }
                dt.Rows.Add(dr);
            }

            {
                DataRow dr = dt.NewRow();
                for (int m = 0; m < 60; m++)
                {
                    DataColumn dc = dt.Columns[m];
                    dr[dc.ToString()] = MemoryParser.MemoryMuncherB.Battery_voltage[x, m];
                }
                dt.Rows.Add(dr);
            }

            {
                DataRow dr = dt.NewRow();
                for (int m = 0; m < 60; m++)
                {
                    DataColumn dc = dt.Columns[m];
                    dr[dc.ToString()] = MemoryParser.MemoryMuncherB.Max_cell_volt[x, m];
                }
                dt.Rows.Add(dr);
            }

            {
                DataRow dr = dt.NewRow();
                for (int m = 0; m < 60; m++)
                {
                    DataColumn dc = dt.Columns[m];
                    dr[dc.ToString()] = MemoryParser.MemoryMuncherB.Module_Identifier_Vmax[x, m];
                }
                dt.Rows.Add(dr);
            }

            {
                DataRow dr = dt.NewRow();
                for (int m = 0; m < 60; m++)
                {
                    DataColumn dc = dt.Columns[m];
                    dr[dc.ToString()] = MemoryParser.MemoryMuncherB.Min_cell_voltage[x, m];
                }
                dt.Rows.Add(dr);
            }

            {
                DataRow dr = dt.NewRow();
                for (int m = 0; m < 60; m++)
                {
                    DataColumn dc = dt.Columns[m];
                    dr[dc.ToString()] = MemoryParser.MemoryMuncherB.Module_Identifier_Vmin[x, m];
                }
                dt.Rows.Add(dr);
            }

            {
                DataRow dr = dt.NewRow();
                for (int m = 0; m < 60; m++)
                {
                    DataColumn dc = dt.Columns[m];
                    dr[dc.ToString()] = MemoryParser.MemoryMuncherB.Max_temp[x, m];
                }
                dt.Rows.Add(dr);
            }

            {
                DataRow dr = dt.NewRow();
                for (int m = 0; m < 60; m++)
                {
                    DataColumn dc = dt.Columns[m];
                    dr[dc.ToString()] = MemoryParser.MemoryMuncherB.Module_Identifier_Tmax[x, m];
                }
                dt.Rows.Add(dr);
            }

            {
                DataRow dr = dt.NewRow();
                for (int m = 0; m < 60; m++)
                {
                    DataColumn dc = dt.Columns[m];
                    dr[dc.ToString()] = MemoryParser.MemoryMuncherB.Min_temp[x, m];
                }
                dt.Rows.Add(dr);
            }

            {
                DataRow dr = dt.NewRow();
                for (int m = 0; m < 60; m++)
                {
                    DataColumn dc = dt.Columns[m];
                    dr[dc.ToString()] = MemoryParser.MemoryMuncherB.Module_Identifier_Tmin[x, m];
                }
                dt.Rows.Add(dr);
            }

            {
                DataRow dr = dt.NewRow();
                for (int m = 0; m < 60; m++)
                {
                    DataColumn dc = dt.Columns[m];
                    dr[dc.ToString()] = MemoryParser.MemoryMuncherB.Avg_temp[x, m];
                }
                dt.Rows.Add(dr);
            }

            {
                for (int i = 0; i < 10; i++)
                {
                    DataRow dr = dt.NewRow();
                    for (int m = 0; m < 60; m++)
                    {
                        DataColumn dc = dt.Columns[m];
                        dr[dc.ToString()] = MemoryParser.MemoryMuncherB.alarms[i, x, m];
                    }
                    dt.Rows.Add(dr);
                }
            }

            {
                DataRow dr = dt.NewRow();
                for (int m = 0; m < 60; m++)
                {
                    DataColumn dc = dt.Columns[m];
                    dr[dc.ToString()] = MemoryParser.MemoryMuncherB.Pbit[x, m];
                }
                dt.Rows.Add(dr);
            }

            {
                DataRow dr = dt.NewRow();
                for (int m = 0; m < 60; m++)
                {
                    DataColumn dc = dt.Columns[m];
                    dr[dc.ToString()] = MemoryParser.MemoryMuncherB.Cbit[x, m];
                }
                dt.Rows.Add(dr);
            }

            {
                DataRow dr = dt.NewRow();
                for (int m = 0; m < 60; m++)
                {
                    DataColumn dc = dt.Columns[m];
                    dr[dc.ToString()] = MemoryParser.MemoryMuncherB.Ibit[x, m];
                }
                dt.Rows.Add(dr);
            }

            {
                DataRow dr = dt.NewRow();
                for (int m = 0; m < 60; m++)
                {
                    DataColumn dc = dt.Columns[m];
                    dr[dc.ToString()] = MemoryParser.MemoryMuncherB.POffBit[x, m];
                }
                dt.Rows.Add(dr);
            }

            {
                DataRow dr = dt.NewRow();
                for (int m = 0; m < 60; m++)
                {
                    DataColumn dc = dt.Columns[m];
                    dr[dc.ToString()] = MemoryParser.MemoryMuncherB.Contactor_status_main_pos[x, m];
                }
                dt.Rows.Add(dr);
            }
            return dt;
        }

        /* Open the Excel file which is generated after parsing the S19 file from GUI*/
        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                string file = System.IO.Path.GetFileNameWithoutExtension(Program.form1.listView.FocusedItem.Text) + ".xlsx";

                string strA = "\\";
                string path = String.Concat(strA, file);//concatenate "\\" with filename
                string pathFile = String.Concat(Program.form1.txtPath.Text, path);// Program.form1.txtPath.Text is directory of excel file
                if ((path != null) && (readfile.address >= 0x080C0000))
                {
                    //These lines will open it in Excel
                    ProcessStartInfo pi = new ProcessStartInfo(pathFile);
                    Process.Start(pi);                    
                }
            }
            catch(Exception)
            {

            }
        }

        aboutForm popup = new aboutForm(); //create instance
        /* It provides the information about tool */
        private void button2_Click_1(object sender, EventArgs e)
        {            
            //popup.Show(); //display popup
            if (!popup.Visible)
            {
                popup.Show();
            }
            else
            {
                popup.BringToFront();
            }
        }
     
    }

    /* Objects for section 1 and section 2 */
    static class MemoryParser
    {
        public static Readbuffer MemoryMuncher = new Readbuffer();
        public static Readbuffer MemoryMuncherB = new Readbuffer();
    }

    /* Read the S19 file */
    /* new class scope outside form */
    public class readfile
    {
        const int COLUMN_WIDTH = 8;
        public Boolean BIG_ENDIAN = false;
        public static ulong address;
        static int column_width = 0;
        static byte[] memory;
        static UInt32 startAddress;
        static UInt32 sectionIndex = 0;
        static UInt32 byteCounter = 0;
        public readfile(string readFName,
            Int32 fileSize,
            UInt32 sAddress)
        {
            int first_data_byte;
            int in_byte_count;
            uint address_size;
            ulong endAddress;
            string inputline;
            sectionIndex = 0;
            byteCounter = 0;
            startAddress = sAddress;
            memory = new byte[fileSize];
            //fill memory
            for (int i = 0; i < fileSize; i++)
            {
                memory[i] = 0;
            }
            StreamReader readStream = File.OpenText(readFName);
            while ((inputline = readStream.ReadLine()) != null)
            {
                if (inputline.Substring(0, 1) == "S")  /* ignore lines not starting */
                /* the letter 'S'            */
                {
                    switch (inputline.Substring(1, 1))
                    {
                        case "1":
                            first_data_byte = 8;
                            address_size = 4;
                            address = Convert.ToUInt64(
                                inputline.Substring(4, 4), 16);
                            convert_line(1, inputline, address_size);
                            break;
                        case "2":
                            first_data_byte = 10;
                            address_size = 6;
                            address = Convert.ToUInt64(
                                inputline.Substring(4, 6), 16);
                            convert_line(2, inputline, address_size);
                            break;
                        case "3":
                            first_data_byte = 12;
                            address_size = 8;
                            address = Convert.ToUInt64(
                                 inputline.Substring(4, 8), 16);
                            if (address >= 0x080C0000)
                            {                         
                                convert_line(2, inputline, address_size);
                            }
                            break;

                        default: break;
                    }
                }
                if (address < 0x080C0000)
                {
                    MessageBox.Show("The S19 file is not compatible with Data Logging Interpreter, Please select appropriate S19 file");
                    break;
                }               
            }
            endAddress = address;/* done reading the whole S19 file */
            readStream.Close();

            /*******************************************************************************/

            /* read memory buffer, assign values based on type to class member variables for section 1*/
            Readbuffer MemoryMuncher = new Readbuffer();
            UInt32 parseAddress = startAddress;
            

            Int32 incrAddr = 0;
            /* start pulling values from memory buffer till all class members are filled */           
            {

                /* assign vars based on type by calling bitconverter */

                for (int m = 0; m < 26; m++)
                {
                    UInt32 i13 = BitConverter.ToUInt32(memory, incrAddr);
                    MemoryParser.MemoryMuncher.Version_bbx[m] = i13;
                    incrAddr += 4;
                    
                    UInt16 i1 = BitConverter.ToUInt16(memory, incrAddr);
                    MemoryParser.MemoryMuncher.Sector_Num[m] = i1;
                    incrAddr += 2;


                    UInt16 i2 = BitConverter.ToUInt16(memory, incrAddr);
                    MemoryParser.MemoryMuncher.Sector_Cur[m] = i2;
                    incrAddr += 2;

                    UInt32 i14 = BitConverter.ToUInt32(memory, incrAddr);
                    MemoryParser.MemoryMuncher.Creation_Date[m] = i14;
                    incrAddr += 4;

                    UInt32 i15 = BitConverter.ToUInt32(memory, incrAddr);
                    MemoryParser.MemoryMuncher.Update_Date[m] = i15;
                    incrAddr += 4;

                    float i31 = BitConverter.ToInt32(memory, incrAddr);
                    MemoryParser.MemoryMuncher.PlusActualWear[m] = i31;
                    incrAddr += 4;

                    float i32 = BitConverter.ToInt32(memory, incrAddr);
                    MemoryParser.MemoryMuncher.MinusActualWear[m] = i32;
                    incrAddr += 4;

                    UInt16 i3 = BitConverter.ToUInt16(memory, incrAddr);
                    MemoryParser.MemoryMuncher.PlusOpeningClosingNum[m] = i3;
                    incrAddr += 2;

                    UInt16 i4 = BitConverter.ToUInt16(memory, incrAddr);
                    MemoryParser.MemoryMuncher.MinusOpeningClosingNum[m] = i4;
                    incrAddr += 2;

                    for (int j = 0; j < 5; j++)
                    {
                        UInt16[] data = new UInt16[5];
                        UInt16 data1 = BitConverter.ToUInt16(memory, incrAddr);
                        data[j] = data1;
                        MemoryParser.MemoryMuncher.PlusOpenClosebyCurrent[j,m] = data[j];
                        incrAddr += 2;
                    }

                    for (int j = 0; j < 5; j++)
                    {
                        UInt16[] data = new UInt16[5];
                        UInt16 data1 = BitConverter.ToUInt16(memory, incrAddr);
                        data[j] = data1;
                        MemoryParser.MemoryMuncher.MinusOpenClosebyCurrent[j,m] = data[j];
                        incrAddr += 2;
                    }

                    UInt32 i16 = BitConverter.ToUInt32(memory, incrAddr);
                    MemoryParser.MemoryMuncher.Bmm_pn[m] = i16;
                    incrAddr += 4;
                    
                    UInt32 i17 = BitConverter.ToUInt32(memory, incrAddr);
                    MemoryParser.MemoryMuncher.Bmm_sn[m] = i17;
                    incrAddr += 4;

                    MemoryParser.MemoryMuncher.Smu_count[m] = memory[incrAddr];
                    incrAddr++;

                    for (int j = 0; j < 40; j++)
                    {
                        UInt32[] data = new UInt32[40];
                        UInt32 data1 = BitConverter.ToUInt32(memory, incrAddr);
                        data[j] = data1;
                        MemoryParser.MemoryMuncher.Smu_sn_Table[j,m] = data[j];
                        incrAddr += 4;
                    }

                    for (int x = 0; x < 11; x++)
                    {
                        MemoryParser.MemoryMuncher.Battery_pn[x, m] = memory[incrAddr];
                        incrAddr++;
                    }


                    for (int y = 0; y < 15; y++)
                    {
                        MemoryParser.MemoryMuncher.Battery_sn[y, m] = memory[incrAddr];
                        incrAddr++;
                    }

                    for (int j = 0; j < 3; j++)
                    {
                        MemoryParser.MemoryMuncher.Sw_version[j,m] = memory[incrAddr];
                        incrAddr++;
                    }
                    /**************************/
                    for (int k = 0; k < 60; k++)
                    {
                        UInt32 i20 = BitConverter.ToUInt32(memory, incrAddr);
                        MemoryParser.MemoryMuncher.Time_stamp[m, k] = i20;
                        incrAddr += 4;

                        MemoryParser.MemoryMuncher.Bmm_mode[m, k] = memory[incrAddr];
                        incrAddr++;

                        MemoryParser.MemoryMuncher.Triggering_event[m, k] = memory[incrAddr];
                        incrAddr++;

                        UInt16 i5 = BitConverter.ToUInt16(memory, incrAddr);
                        MemoryParser.MemoryMuncher.Event_act_Counter[m, k] = i5;
                        incrAddr += 2;

                        MemoryParser.MemoryMuncher.Soc_min[m, k] = memory[incrAddr];
                        incrAddr++;

                        MemoryParser.MemoryMuncher.Soc_max[m, k] = memory[incrAddr];
                        incrAddr++;

                        MemoryParser.MemoryMuncher.Soc_avg[m, k] = memory[incrAddr];
                        incrAddr++;

                        UInt16 i6 = BitConverter.ToUInt16(memory, incrAddr);
                        MemoryParser.MemoryMuncher.Soh[m, k] = i6;
                        incrAddr += 2;

                        float i21 = BitConverter.ToSingle(memory, incrAddr);
                        MemoryParser.MemoryMuncher.Cur_Avg_High_Range[m, k] = i21;
                        incrAddr += 4;

                        float i22 = BitConverter.ToSingle(memory, incrAddr);
                        MemoryParser.MemoryMuncher.Cur_Avg_Low_Range[m, k] = i22;
                        incrAddr += 4;

                        UInt32 i23 = BitConverter.ToUInt32(memory, incrAddr);
                        MemoryParser.MemoryMuncher.Amp_throughput_highrange[m, k] = i23;
                        incrAddr += 4;

                        UInt32 i24 = BitConverter.ToUInt32(memory, incrAddr);
                        MemoryParser.MemoryMuncher.Amp_throughput_lowrange[m, k] = i24;
                        incrAddr += 4;

                        UInt32 i7 = BitConverter.ToUInt32(memory, incrAddr);
                        MemoryParser.MemoryMuncher.Battery_voltage[m, k] = i7;
                        incrAddr += 4;

                        UInt16 i8 = BitConverter.ToUInt16(memory, incrAddr);
                        MemoryParser.MemoryMuncher.Max_cell_volt[m, k] = i8;
                        incrAddr += 2;


                        UInt32 i25 = BitConverter.ToUInt32(memory, incrAddr);
                        MemoryParser.MemoryMuncher.Module_Identifier_Vmax[m, k] = i25;
                        incrAddr += 4;

                        UInt16 i9 = BitConverter.ToUInt16(memory, incrAddr);
                        MemoryParser.MemoryMuncher.Min_cell_voltage[m, k] = i9;
                        incrAddr += 2;

                        UInt32 i26 = BitConverter.ToUInt32(memory, incrAddr);
                        MemoryParser.MemoryMuncher.Module_Identifier_Vmin[m, k] = i26;
                        incrAddr += 4;

                        MemoryParser.MemoryMuncher.Max_temp[m, k] = (sbyte)memory[incrAddr];
                        incrAddr++;

                        UInt32 i27 = BitConverter.ToUInt32(memory, incrAddr);
                        MemoryParser.MemoryMuncher.Module_Identifier_Tmax[m, k] = i27;
                        incrAddr += 4;

                        MemoryParser.MemoryMuncher.Min_temp[m, k] = (sbyte)memory[incrAddr];
                        incrAddr++;

                        UInt32 i28 = BitConverter.ToUInt32(memory, incrAddr);
                        MemoryParser.MemoryMuncher.Module_Identifier_Tmin[m, k] = i28;
                        incrAddr += 4;

                        MemoryParser.MemoryMuncher.Avg_temp[m, k] = (sbyte)memory[incrAddr];
                        incrAddr++;

                        for (int j = 0; j < 10; j++)
                        {
                            MemoryParser.MemoryMuncher.alarms[j, m, k] = memory[incrAddr];
                            incrAddr++;
                        }

                        UInt32 i29 = BitConverter.ToUInt32(memory, incrAddr);
                        MemoryParser.MemoryMuncher.Pbit[m, k] = i29;
                        incrAddr += 4;

                        UInt32 i30 = BitConverter.ToUInt32(memory, incrAddr);
                        MemoryParser.MemoryMuncher.Cbit[m, k] = i30;
                        incrAddr += 4;

                        UInt16 i10 = BitConverter.ToUInt16(memory, incrAddr);
                        MemoryParser.MemoryMuncher.Ibit[m, k] = i10;
                        incrAddr += 2;

                        UInt16 i11 = BitConverter.ToUInt16(memory, incrAddr);
                        MemoryParser.MemoryMuncher.POffBit[m, k] = i11;
                        incrAddr += 2;

                        MemoryParser.MemoryMuncher.Contactor_status_main_pos[m,k] = memory[incrAddr];
                        incrAddr++;
                    }


                    /**********************/
                    UInt32 i41 = BitConverter.ToUInt32(memory, incrAddr);
                    MemoryParser.MemoryMuncher.uiLastSlopeUpdateBatteryCapacity[m] = i41;
                    incrAddr += 4;

                    UInt32 i42 = BitConverter.ToUInt32(memory, incrAddr);
                    MemoryParser.MemoryMuncher.uiLastSlopeUpdateTime[m] = i42;
                    incrAddr += 4;

                    UInt32 i43 = BitConverter.ToUInt32(memory, incrAddr);
                    MemoryParser.MemoryMuncher.uiLastSlopeUpdateModuleSerialNumber[m] = i43;
                    incrAddr += 4;

                    UInt32 i44 = BitConverter.ToUInt32(memory, incrAddr);
                    MemoryParser.MemoryMuncher.uiLastSlopeUpdateModuleInitCapacity[m] = i44;
                    incrAddr += 4;

                    UInt32 i45 = BitConverter.ToUInt32(memory, incrAddr);
                    MemoryParser.MemoryMuncher.uiTimeToWaitFromPessimisticCalculation[m] = i45;
                    incrAddr += 4;

                    UInt32 i46 = BitConverter.ToUInt32(memory, incrAddr);
                    MemoryParser.MemoryMuncher.uiCapacity[m] = i46;
                    incrAddr += 4;

                    UInt32 i47 = BitConverter.ToUInt32(memory, incrAddr);
                    MemoryParser.MemoryMuncher.uiRelativeWear[m] = i47;
                    incrAddr += 4;

                    UInt16 i48 = BitConverter.ToUInt16(memory, incrAddr);
                    MemoryParser.MemoryMuncher.ushCapacitySlope[m] = i48;
                    incrAddr += 2;

                    for (int j = 0; j < 2; j++)
                    {
                        MemoryParser.MemoryMuncher.Padding[j,m] = memory[incrAddr];
                        incrAddr++;
                    }

                    UInt16 i12 = BitConverter.ToUInt16(memory, incrAddr);
                    MemoryParser.MemoryMuncher.Crc16[m] = i12;
                    incrAddr += 2;
                }

                parseAddress += (UInt32)incrAddr;
                
            }

            /*******************************************************************************/
           

            /* read memory buffer, assign values based on type to class member variables for section 2*/
            Readbuffer MemoryMuncherB = new Readbuffer();
            
            UInt32 parseAddressB = startAddress;     

            Int32 incrAddrB = (Int32)sectionIndex;
            /* start pulling values from memory buffer till all class members are filled */           
            {

                /* assign vars based on type by calling bitconverter */
                /**********************/
                for (int m = 0; m < 26; m++)
                {
                    UInt32 i13 = BitConverter.ToUInt32(memory, incrAddrB);
                    MemoryParser.MemoryMuncherB.Version_bbx[m] = i13;
                    incrAddrB += 4;                    

                    UInt16 i1 = BitConverter.ToUInt16(memory, incrAddrB);
                    MemoryParser.MemoryMuncherB.Sector_Num[m] = i1;
                    incrAddrB += 2;


                    UInt16 i2 = BitConverter.ToUInt16(memory, incrAddrB);
                    MemoryParser.MemoryMuncherB.Sector_Cur[m] = i2;
                    incrAddrB += 2;

                    UInt32 i14 = BitConverter.ToUInt32(memory, incrAddrB);
                    MemoryParser.MemoryMuncherB.Creation_Date[m] = i14;
                    incrAddrB += 4;

                    UInt32 i15 = BitConverter.ToUInt32(memory, incrAddrB);
                    MemoryParser.MemoryMuncherB.Update_Date[m] = i15;
                    incrAddrB += 4;

                    float i31 = BitConverter.ToInt32(memory, incrAddrB);
                    MemoryParser.MemoryMuncherB.PlusActualWear[m] = i31;
                    incrAddrB += 4;

                    float i32 = BitConverter.ToInt32(memory, incrAddrB);
                    MemoryParser.MemoryMuncherB.MinusActualWear[m] = i32;
                    incrAddrB += 4;

                    UInt16 i3 = BitConverter.ToUInt16(memory, incrAddrB);
                    MemoryParser.MemoryMuncherB.PlusOpeningClosingNum[m] = i3;
                    incrAddrB += 2;

                    UInt16 i4 = BitConverter.ToUInt16(memory, incrAddrB);
                    MemoryParser.MemoryMuncherB.MinusOpeningClosingNum[m] = i4;
                    incrAddrB += 2;

                    for (int j = 0; j < 5; j++)
                    {
                        UInt16[] data = new UInt16[5];
                        UInt16 data1 = BitConverter.ToUInt16(memory, incrAddrB);
                        data[j] = data1;
                        MemoryParser.MemoryMuncherB.PlusOpenClosebyCurrent[j,m] = data[j];
                        incrAddrB += 2;
                    }

                    for (int j = 0; j < 5; j++)
                    {
                        UInt16[] data = new UInt16[5];
                        UInt16 data1 = BitConverter.ToUInt16(memory, incrAddrB);
                        data[j] = data1;
                        MemoryParser.MemoryMuncherB.MinusOpenClosebyCurrent[j,m] = data[j];
                        incrAddrB += 2;
                    }

                    UInt32 i16 = BitConverter.ToUInt32(memory, incrAddrB);
                    MemoryParser.MemoryMuncherB.Bmm_pn[m] = i16;
                    incrAddrB += 4;              


                    UInt32 i17 = BitConverter.ToUInt32(memory, incrAddrB);
                    MemoryParser.MemoryMuncherB.Bmm_sn[m] = i17;
                    incrAddrB += 4;

                    MemoryParser.MemoryMuncherB.Smu_count[m] = memory[incrAddrB];
                    incrAddrB++;

                    for (int j = 0; j < 40; j++)
                    {
                        UInt32[] data = new UInt32[40];
                        UInt32 data1 = BitConverter.ToUInt32(memory, incrAddrB);
                        data[j] = data1;
                        MemoryParser.MemoryMuncherB.Smu_sn_Table[j,m] = data[j];
                        incrAddrB += 4;
                    }

                    for (int x = 0; x < 11; x++)
                    {
                        MemoryParser.MemoryMuncherB.Battery_pn[x,m] = memory[incrAddrB];
                        incrAddrB++;
                    }


                    for (int y = 0; y < 15; y++)
                    {
                        MemoryParser.MemoryMuncherB.Battery_sn[y,m] = memory[incrAddrB];
                        incrAddrB++;
                    }

                    for (int j = 0; j < 3; j++)
                    {
                        MemoryParser.MemoryMuncherB.Sw_version[j,m] = memory[incrAddrB];
                        incrAddrB++;
                    }

                    for (int k = 0; k < 60; k++)
                    {
                        UInt32 i20 = BitConverter.ToUInt32(memory, incrAddrB);
                        MemoryParser.MemoryMuncherB.Time_stamp[m, k] = i20;
                        incrAddrB += 4;

                        MemoryParser.MemoryMuncherB.Bmm_mode[m, k] = memory[incrAddrB];
                        incrAddrB++;

                        MemoryParser.MemoryMuncherB.Triggering_event[m, k] = memory[incrAddrB];
                        incrAddrB++;

                        UInt16 i5 = BitConverter.ToUInt16(memory, incrAddrB);
                        MemoryParser.MemoryMuncherB.Event_act_Counter[m, k] = i5;
                        incrAddrB += 2;

                        MemoryParser.MemoryMuncherB.Soc_min[m, k] = memory[incrAddrB];
                        incrAddrB++;

                        MemoryParser.MemoryMuncherB.Soc_max[m, k] = memory[incrAddrB];
                        incrAddrB++;

                        MemoryParser.MemoryMuncherB.Soc_avg[m, k] = memory[incrAddrB];
                        incrAddrB++;

                        UInt16 i6 = BitConverter.ToUInt16(memory, incrAddrB);
                        MemoryParser.MemoryMuncherB.Soh[m, k] = i6;
                        incrAddrB += 2;

                        float i21 = BitConverter.ToSingle(memory, incrAddrB);
                        MemoryParser.MemoryMuncherB.Cur_Avg_High_Range[m, k] = i21;
                        incrAddrB += 4;

                        float i22 = BitConverter.ToSingle(memory, incrAddrB);
                        MemoryParser.MemoryMuncherB.Cur_Avg_Low_Range[m, k] = i22;
                        incrAddrB += 4;

                        UInt32 i23 = BitConverter.ToUInt32(memory, incrAddrB);
                        MemoryParser.MemoryMuncherB.Amp_throughput_highrange[m, k] = i23;
                        incrAddrB += 4;

                        UInt32 i24 = BitConverter.ToUInt32(memory, incrAddrB);
                        MemoryParser.MemoryMuncherB.Amp_throughput_lowrange[m, k] = i24;
                        incrAddrB += 4;

                        UInt32 i7 = BitConverter.ToUInt32(memory, incrAddrB);
                        MemoryParser.MemoryMuncherB.Battery_voltage[m, k] = i7;
                        incrAddrB += 4;

                        UInt16 i8 = BitConverter.ToUInt16(memory, incrAddrB);
                        MemoryParser.MemoryMuncherB.Max_cell_volt[m, k] = i8;
                        incrAddrB += 2;


                        UInt32 i25 = BitConverter.ToUInt32(memory, incrAddrB);
                        MemoryParser.MemoryMuncherB.Module_Identifier_Vmax[m, k] = i25;
                        incrAddrB += 4;

                        UInt16 i9 = BitConverter.ToUInt16(memory, incrAddrB);
                        MemoryParser.MemoryMuncherB.Min_cell_voltage[m, k] = i9;
                        incrAddrB += 2;

                        UInt32 i26 = BitConverter.ToUInt32(memory, incrAddrB);
                        MemoryParser.MemoryMuncherB.Module_Identifier_Vmin[m, k] = i26;
                        incrAddrB += 4;

                        MemoryParser.MemoryMuncherB.Max_temp[m, k] = (sbyte)memory[incrAddrB];
                        incrAddrB++;

                        UInt32 i27 = BitConverter.ToUInt32(memory, incrAddrB);
                        MemoryParser.MemoryMuncherB.Module_Identifier_Tmax[m, k] = i27;
                        incrAddrB += 4;

                        MemoryParser.MemoryMuncherB.Min_temp[m, k] = (sbyte)memory[incrAddrB];
                        incrAddrB++;

                        UInt32 i28 = BitConverter.ToUInt32(memory, incrAddrB);
                        MemoryParser.MemoryMuncherB.Module_Identifier_Tmin[m, k] = i28;
                        incrAddrB += 4;

                        MemoryParser.MemoryMuncherB.Avg_temp[m, k] = (sbyte)memory[incrAddrB];
                        incrAddrB++;

                        for (int j = 0; j < 10; j++)
                        {
                            MemoryParser.MemoryMuncherB.alarms[j, m, k] = memory[incrAddrB];
                            incrAddrB++;
                        }

                        UInt32 i29 = BitConverter.ToUInt32(memory, incrAddrB);
                        MemoryParser.MemoryMuncherB.Pbit[m, k] = i29;
                        incrAddrB += 4;

                        UInt32 i30 = BitConverter.ToUInt32(memory, incrAddrB);
                        MemoryParser.MemoryMuncherB.Cbit[m, k] = i30;
                        incrAddrB += 4;

                        UInt16 i10 = BitConverter.ToUInt16(memory, incrAddrB);
                        MemoryParser.MemoryMuncherB.Ibit[m, k] = i10;
                        incrAddrB += 2;

                        UInt16 i11 = BitConverter.ToUInt16(memory, incrAddrB);
                        MemoryParser.MemoryMuncherB.POffBit[m, k] = i11;
                        incrAddrB += 2;

                        MemoryParser.MemoryMuncherB.Contactor_status_main_pos[m, k] = memory[incrAddrB];
                        incrAddrB++;
                    }


                    /**********************/
                    UInt32 i41 = BitConverter.ToUInt32(memory, incrAddrB);
                    MemoryParser.MemoryMuncherB.uiLastSlopeUpdateBatteryCapacity[m] = i41;
                    incrAddrB += 4;

                    UInt32 i42 = BitConverter.ToUInt32(memory, incrAddrB);
                    MemoryParser.MemoryMuncherB.uiLastSlopeUpdateTime[m] = i42;
                    incrAddrB += 4;

                    UInt32 i43 = BitConverter.ToUInt32(memory, incrAddrB);
                    MemoryParser.MemoryMuncherB.uiLastSlopeUpdateModuleSerialNumber[m] = i43;
                    incrAddrB += 4;

                    UInt32 i44 = BitConverter.ToUInt32(memory, incrAddrB);
                    MemoryParser.MemoryMuncherB.uiLastSlopeUpdateModuleInitCapacity[m] = i44;
                    incrAddrB += 4;

                    UInt32 i45 = BitConverter.ToUInt32(memory, incrAddrB);
                    MemoryParser.MemoryMuncherB.uiTimeToWaitFromPessimisticCalculation[m] = i45;
                    incrAddrB += 4;

                    UInt32 i46 = BitConverter.ToUInt32(memory, incrAddrB);
                    MemoryParser.MemoryMuncherB.uiCapacity[m] = i46;
                    incrAddrB += 4;

                    UInt32 i47 = BitConverter.ToUInt32(memory, incrAddrB);
                    MemoryParser.MemoryMuncherB.uiRelativeWear[m] = i47;
                    incrAddrB += 4;

                    UInt16 i48 = BitConverter.ToUInt16(memory, incrAddrB);
                    MemoryParser.MemoryMuncherB.ushCapacitySlope[m] = i48;
                    incrAddrB += 2;

                    for (int j = 0; j < 2; j++)
                    {
                        MemoryParser.MemoryMuncherB.Padding[j,m] = memory[incrAddrB];
                        incrAddrB++;
                    }

                    UInt16 i12 = BitConverter.ToUInt16(memory, incrAddrB);
                    MemoryParser.MemoryMuncherB.Crc16[m] = i12;
                    incrAddrB += 2;
                }

                parseAddressB += (UInt32)incrAddrB;
            }

            /*******************************************************************************/           


        }


        /* Getting the S19 string from memory */
        void convert_line(uint numberBytes, string inputline, uint address_size)
        {
            uint columncount;
            int in_byte_count;
            short data_word=0;
            in_byte_count = Convert.ToInt16(
                 inputline.Substring(2, 2), 16);

            for (columncount = address_size + 4;
               columncount < (2*in_byte_count)+4;
               columncount += numberBytes)
            {
                switch (numberBytes)
                {
                    case 1:
                        data_word = Convert.ToByte(
                           inputline.Substring((int)columncount, 1), 16);
                        memory[address - startAddress] = (byte)(data_word & 0xff);
                        break;
                    case 2:
                        data_word = Convert.ToInt16(
                           inputline.Substring((int)columncount, 2), 16);                        
                        if(!BIG_ENDIAN)
                        {
                           memory[address - startAddress] = (byte)(data_word & 0xff);
                        }
                        byteCounter++;
                        
                        if (address == 0x080E0000)
                        {
                            /* 0x2001 is the number of lines from 0x80c0000, needs to subtract from byteCounter */
                            sectionIndex = (byteCounter - 0x2001);
                        }                        
                        break;
                    case 4:
                        data_word = Convert.ToInt16(
                           inputline.Substring((int)columncount, 8), 16);
                        if (BIG_ENDIAN)
                        {
                            memory[address - startAddress] = (byte)((data_word >> 6) & 0xff);
                            memory[address - startAddress + 1] = (byte)((data_word >> 4) & 0xff);
                            memory[address - startAddress + 2] = (byte)((data_word >> 2) & 0xff);
                            memory[address - startAddress + 3] = (byte)(data_word & 0xff);
                        }
                        else
                        {
                            memory[address - startAddress] = (byte)(data_word & 0xff);
                            memory[address - startAddress + 1] = (byte)((data_word >> 2) & 0xff);
                            memory[address - startAddress + 2] = (byte)((data_word >> 4) & 0xff);
                            memory[address - startAddress + 3] = (byte)((data_word >> 6) & 0xff);
                        }
                        break;
                }
                               
                column_width++;
                if(column_width == COLUMN_WIDTH)
                {
                    Console.WriteLine("");
                    column_width = 0;
                }
                address += numberBytes/2; 
            }
        }
    }
}

/* Class members to store the S19 data from memory parser */ 
public class Readbuffer
{
    const UInt32 uiSixtySeconds = 60;

    public UInt32[] Version_bbx = new UInt32[26];
    public UInt16[] Sector_Num = new UInt16[26];
    public UInt16[] Sector_Cur = new UInt16[26];
    public UInt32[] Creation_Date = new UInt32[26];
    public UInt32[] Update_Date = new UInt32[26];
    public float[] PlusActualWear = new float[26];
    public float[] MinusActualWear = new float[26];
    public UInt16[] PlusOpeningClosingNum = new UInt16[26];
    public UInt16[] MinusOpeningClosingNum = new UInt16[26];
    public UInt16[,] PlusOpenClosebyCurrent = new UInt16[5,26];
    public UInt16[,] MinusOpenClosebyCurrent = new UInt16[5,26];
    public UInt32[] Bmm_pn = new UInt32[26];
    public UInt32[] Bmm_sn = new UInt32[26];
    public byte[] Smu_count = new byte[26];
    public UInt32[,] Smu_sn_Table = new UInt32[40,26];
    public byte[,] Battery_pn = new byte[11,26];
    public byte[,] Battery_sn = new byte[15,26];
    public byte[,] Sw_version = new byte[3,26];

    public UInt32[,] Time_stamp = new UInt32[26,uiSixtySeconds];
    public byte[,] Bmm_mode = new byte[26, uiSixtySeconds];
    public byte[,] Triggering_event = new byte[26, uiSixtySeconds];
    public UInt16[,] Event_act_Counter = new UInt16[26, uiSixtySeconds];
    public byte[,] Soc_min = new byte[26, uiSixtySeconds];
    public byte[,] Soc_max = new byte[26, uiSixtySeconds];
    public byte[,] Soc_avg = new byte[26, uiSixtySeconds];
    public UInt16[,] Soh = new UInt16[26, uiSixtySeconds];
    public float[,] Cur_Avg_High_Range = new float[26, uiSixtySeconds];
    public float[,] Cur_Avg_Low_Range = new float[26, uiSixtySeconds];
    public UInt32[,] Amp_throughput_highrange = new UInt32[26, uiSixtySeconds];
    public UInt32[,] Amp_throughput_lowrange = new UInt32[26, uiSixtySeconds];
    public UInt32[,] Battery_voltage = new UInt32[26, uiSixtySeconds];
    public UInt16[,] Max_cell_volt = new UInt16[26, uiSixtySeconds];
    public UInt32[,] Module_Identifier_Vmax = new UInt32[26, uiSixtySeconds];
    public UInt16[,] Min_cell_voltage = new UInt16[26, uiSixtySeconds];
    public UInt32[,] Module_Identifier_Vmin = new UInt32[26, uiSixtySeconds];
    public sbyte[,] Max_temp = new sbyte[26, uiSixtySeconds];
    public UInt32[,] Module_Identifier_Tmax = new UInt32[26, uiSixtySeconds];
    public sbyte[,] Min_temp = new sbyte[26, uiSixtySeconds];
    public UInt32[,] Module_Identifier_Tmin = new UInt32[26, uiSixtySeconds];
    public sbyte[,] Avg_temp = new sbyte[26, uiSixtySeconds];
    public byte[,,] alarms = new byte[10,26, uiSixtySeconds];
    public UInt32[,] Pbit = new UInt32[26, uiSixtySeconds];
    public UInt32[,] Cbit = new UInt32[26, uiSixtySeconds];
    public UInt16[,] Ibit = new UInt16[26, uiSixtySeconds];
    public UInt16[,] POffBit = new UInt16[26, uiSixtySeconds];
    public byte[,] Contactor_status_main_pos = new byte[26, uiSixtySeconds];

    public UInt32[] uiLastSlopeUpdateBatteryCapacity = new UInt32[26];
    public UInt32[] uiLastSlopeUpdateTime = new UInt32[26];
    public UInt32[] uiLastSlopeUpdateModuleSerialNumber = new UInt32[26];
    public UInt32[] uiLastSlopeUpdateModuleInitCapacity = new UInt32[26];
    public UInt32[] uiTimeToWaitFromPessimisticCalculation = new UInt32[26];
    public UInt32[] uiCapacity = new UInt32[26];
    public UInt32[] uiRelativeWear = new UInt32[26];
    public UInt16[] ushCapacitySlope = new UInt16[26];
    public byte[,] Padding = new byte[2,26];
    public UInt16[] Crc16 = new UInt16[26];
}

