# README #

Software Purpose:
This tool developed for interpreter is limited to Intensium HE only. 
It will convert S19 file to MS Excel file. It display the data in GUI and also user can open the excel file from GUI.

DLLs:
Costura.Fody.dll v3.3.3
EPPlus.dll       v4.5.3.1
Fody.dll         V4.0.2

List steps on how to build the software:
1. Go to Project -> Manage Nuget Packages -> Browse ->EPPlus install file
2. Go to Project -> Manage Nuget Packages -> Browse ->Costura.Fody install file
3. Go to Project -> Manage Nuget Packages -> Browse ->Fody install file
4. Select Release in Solution Configurations
5. Select any CPU for Solution Platforms
6. Click on start
7. Click on Continue debugging
8. Parsing_File.exe will be generated.

List steps on how to run the generated binary from the build process:
1. Run ..\Parsing_File\Parsing_File\bin\Release\Parsing_File.exe from the system.
2. Click on Open button to select the S19 file in appropriate folder. 
3. If you select wrong excel file click on export button will through message for selecting the correct S19 file.
4. Select S19 file click on Export, it will generate excel file. Also it diplay the excel sheets in GUI.
5. Click on Open Excel file, which opens the excel file.


Host Operating System:
Windows 10

Processor Architectures:
 x86 64-bit


Visual Studio Version: VS2017, version 15.5
MSVC Toolset Version:  v141 in VS; version 14.12
Compiler(s) version(s) selected to build executable: MSVC compiler version 1912

Include: copyright (C) Copyright 2019 by SAFT. All rights reserved
